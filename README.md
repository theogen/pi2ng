# PI2NG

![banner](media/banner.png)

Supercharge the crystal and see what happens! Be careful, it's very unstable!
You have to contain it, otherwise it will get destroyed.

This is a simple radial pong where you control both rackets simultaniously.


## Controls

\<D\>, \<Right Arrow\>, \<L\>, \<;\> or \<Left Joystick\> - rotate clockwise.

\<A\>, \<Left Arrow\>, \<H\>, \<J\> or \<Left Joystick\> - rotate
counterclockwise.

\<M\> - mute music.

\<F\> - disable flashing.

\<Escape\> or \<Q\> - exit the game at any time. Keep in mind that your
progress won't be saved!


## Credits

See [`CREDITS.md`](./CREDITS.md).
