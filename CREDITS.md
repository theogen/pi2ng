# Credits

[jam]: https://itch.io/jam/ccc-jam
[love]: https://love2d.org
[font]: https://fonts.google.com/specimen/Major+Mono+Display
[music]: https://freesound.org/people/frankum/sounds/394286
[crystal]: https://freesound.org/people/caiogracco
[break]: https://freesound.org/people/unfa/sounds/221528
[explosion]: https://freesound.org/people/V-ktor/sounds/482993
[palette]: https://lospec.com/palette-list/pico-8

This game has originally been developed by Theogen Ratkin for the
[Content Creators Community Jam 2020][jam].

Engine: [LÖVE framework][love]

Font: [Major Mono Display by Emre Parlak][font]

Music: ["sad Waltz" by frankum][music]

Crystal sounds by [caiogracco][crystal]

Crystal break sound: [Glass Break by unfa][break]

Explosion sound: [large explosion 1 by V-ktor][explosion]

Palette: [PICO-8 Palette][palette]
